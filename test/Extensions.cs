﻿using Android.App;
using Android.Content;
using Android.Graphics;
using System;
using Uri = Android.Net.Uri;

namespace test
{
    public static class Extensions
    {
        public static void StartNewActivity(this Activity activity, Type newActivity)
        {
            using (var intent = new Intent(activity, newActivity))
            {
                activity.StartActivity(intent);
            }
        }

        public static int CalculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight)
        {
            // Raw height and width of image
            var height = (float)options.OutHeight;
            var width = (float)options.OutWidth;
            var inSampleSize = 1D;

            if (height > reqHeight || width > reqWidth)
            {
                inSampleSize = width > height
                    ? height / reqHeight
                    : width / reqWidth;
            }

            return (int)inSampleSize;
        }

        public static Bitmap DecodeSampledBitmapFromResource(this string path, int reqWidth, int reqHeight)
        {
            // First decode with inJustDecodeBounds=true to check dimensions
            var options = new BitmapFactory.Options
            {
                InJustDecodeBounds = true,
            };
            using (var dispose = BitmapFactory.DecodeFile(path, options))
            {
            }

            // Calculate inSampleSize
            options.InSampleSize = CalculateInSampleSize(options, reqWidth, reqHeight);

            // Decode bitmap with inSampleSize set
            options.InJustDecodeBounds = false;
            return BitmapFactory.DecodeFile(path, options);
        }
    }
}
