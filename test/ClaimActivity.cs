using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using File = System.IO.File;
using Uri = Android.Net.Uri;

namespace test
{
	[Activity (Label = "test", MainLauncher = false)]
	public class ClaimActivity : Activity
	{

		private LinearLayout _layout;

	    private List<Uri> _images;

	    private Gallery _gallery;

	    private List<ImageView> _imageViews;

	    private FileFinder _fileFinder;

	    protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);
			SetContentView(Resource.Layout.Claiming);
            _fileFinder = new FileFinder(this);
            
		    _layout = FindViewById<LinearLayout>(Resource.Id.MainLinearLayout);
            _images = new List<Uri>();
		    _gallery = new Gallery(_layout.Context) {LayoutParameters = new Gallery.LayoutParams(600, 600)};
	        _layout.AddView(_gallery);
            _imageViews = new List<ImageView>();

			var uploadButton = FindViewById<Button>(Resource.Id.myButton);

		    uploadButton.Click += UploadButtonClick;

            uploadButton.Visibility = ViewStates.Invisible;
            
            var selectImageBtn = FindViewById<Button>(Resource.Id.SelectImageBtn);

            selectImageBtn.Click += SelectImage;
		}

	    private void UploadButtonClick(object obj, EventArgs args)
	    {
	        var list = new List<byte[]>();
	        foreach (var image in _images)
	        {
                var path = _fileFinder.GetPathToImage(image);
	            using (var file = File.OpenRead(path))
	            {
	                var buffer = new byte[file.Length];
                    file.Read(buffer, 0, buffer.Length);
                    file.Close();
                    list.Add(buffer);
	            }
	        }
	    }

        private void ImageItemClick(object obj, EventArgs args)
        {
            var itemClickArgs = (AdapterView.ItemClickEventArgs) args;
            var position = itemClickArgs.Position;
            var uri = _images[position];
            Toast.MakeText(this, String.Format("you clicked {0}!", uri.ToString()), ToastLength.Short).Show();
        }

        private void SelectImage(object obj, EventArgs args)
	    {
            var imageIntent = new Intent();
            imageIntent.SetType("image/*");
            imageIntent.SetAction(Intent.ActionGetContent);
            StartActivityForResult(Intent.CreateChooser(imageIntent, "Select photo"), 0);
	    }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            if (resultCode != Result.Ok || _images.Contains(data.Data)) return;

            var uploadImgBtn = FindViewById<Button>(Resource.Id.myButton);
            uploadImgBtn.Visibility = ViewStates.Visible;

            _images.Add(data.Data);

            var imageView = new ImageView(this) {LayoutParameters = new Gallery.LayoutParams(600, 600)};
			imageView.SetScaleType(ImageView.ScaleType.FitXy);
            
			using (var bmp = _fileFinder.GetPathToImage(data.Data).DecodeSampledBitmapFromResource(300, 300)) 
			{
				imageView.SetImageBitmap(bmp);
			}       

            _imageViews.Add(imageView);
            if(_gallery.Adapter != null)
                _gallery.Adapter.Dispose();
            _gallery.Adapter = new ImageViewAdapter(this, ref _imageViews);
        }

	    
	}
}


