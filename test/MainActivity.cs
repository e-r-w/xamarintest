using Android.App;
using Android.OS;
using Android.Widget;
using System;

namespace test
{
	[Activity (Label = "test2", MainLauncher = true)]
	public class MainAppActivity : Activity
	{

	    protected override void OnCreate(Bundle bundle)
	    {
	        base.OnCreate(bundle);
			SetContentView(Resource.Layout.Main);


            var selectImageBtn = FindViewById<Button>(Resource.Id.continueBtn);

            selectImageBtn.Click += ContinueClick;
	    }

	    private void ContinueClick(object sender, EventArgs eventArgs)
	    {
            this.StartNewActivity(typeof(ClaimActivity));
	    }

	}
}


