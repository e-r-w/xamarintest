﻿using Android.App;
using Android.Net;

namespace test
{
    public class FileFinder
    {
        private readonly Activity _activity;

        public FileFinder(Activity activity)
        {
            _activity = activity;
        }

        public string GetPathToImage(Uri uri)
        {
            string docId;
            using (var c1 = _activity.ContentResolver.Query(uri, null, null, null, null))
            {
                c1.MoveToFirst();
                var documentId = c1.GetString(0);
                docId = documentId.Substring(documentId.LastIndexOf(":") + 1);
            }

            string path;

            // The projection contains the columns we want to return in our query.
            const string selection = Android.Provider.MediaStore.Images.Media.InterfaceConsts.Id + " =? ";
            using (var cursor = _activity.ManagedQuery(Android.Provider.MediaStore.Images.Media.ExternalContentUri, null, selection, new[] { docId }, null))
            {
                if (cursor == null) return null;
                var columnIndex = cursor.GetColumnIndexOrThrow(Android.Provider.MediaStore.Images.Media.InterfaceConsts.Data);
                cursor.MoveToFirst();
                path = cursor.GetString(columnIndex);
            }
            return path;
        }
    }
}