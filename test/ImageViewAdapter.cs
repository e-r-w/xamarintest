using Android.App;
using Android.Views;
using Android.Widget;
using System.Collections.Generic;
using Object = Java.Lang.Object;

namespace test
{
    public class ImageViewAdapter : BaseAdapter
    {

        public List<ImageView> ImageViews { get; set; }

        private readonly Activity _context;

        public ImageViewAdapter(Activity context)
        {
            _context = context;
            ImageViews = new List<ImageView>();
        }

        public ImageViewAdapter(Activity context, ref List<ImageView> images)
        {
            _context = context;
            ImageViews = images;
        }

        public override Object GetItem(int position)
        {
			return ImageViews[position];
        }

        public override long GetItemId(int position)
        {
            return 0;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            return ImageViews[position];
        }

        public override int Count
        {
            get { return ImageViews.Count; }
        }
            
    }
}